#!/usr/bin/env bash

apt-get update
# MySql Server
debconf-set-selections <<< 'mysql-server mysql-server/root_password password 123'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password 123'
apt-get -y install mysql-server

# Packages
apt-get install -y apache2 php5 php5-intl php5-curl vim php5-mysql php-pear php5-dev git-core node php-apc

# Node JS
curl -sL https://deb.nodesource.com/setup | bash -
apt-get install -y nodejs

# NPM
curl https://www.npmjs.org/install.sh | sudo sh
npm install -g less@1.7.5 -y
update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10

# PHPMYADMIN
debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password 123' 
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password 123'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password 123' 
debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
apt-get install -y phpmyadmin

# Composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Apache Modules
printf "\ndate.timezone = Africa/Tunis" >> /etc/php5/apache2/php.ini
printf "\nzend_extension=\"/usr/local/php/modules/xdebug.so\"" >> /etc/php5/apache2/php.ini
printf "\nInclude /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf

sudo a2enmod rewrite
sudo service apache2 restart
